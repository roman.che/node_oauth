const express = require('express')
const router = express.Router()
const user = require('./user');
const auth = require('./auth');
const {badRoute} = require('../common/middleware');

router
    .use('/auth', auth)
    .use('/user', user)
    .use(badRoute);

module.exports = router;