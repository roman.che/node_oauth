const express = require('express')
const router = express.Router();
const base = require('./base');
const google = require('./google');
const {badRoute} = require('../../../common/middleware');

router
    .use('/base', base)
    .use('/google', google)
    .use(badRoute);

module.exports = router;