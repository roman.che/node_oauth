const express = require('express');
const router = express.Router();
const {successResponse} = require('../../../common/response');
const {badRoute, findByEmail, verifyPassword} = require('../../../common/middleware');

router
    .post('/login', findByEmail, verifyPassword, login)
    .use(badRoute);

function login(request, response) {
    return successResponse(response, {message: `Hello from Base Auth Provider`});
}

module.exports = router;