const express = require('express');
const router = express.Router();
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth2').Strategy;
const {successResponse, errorResponse} = require('../../../common/response');
const {badRoute, findByEmail, verifyPassword} = require('../../../common/middleware');
const {googleClientId, googleClientSecret, googleCallbackUrl} = require('../../../config');
const {loginForm} = require('../../../views/form');
const {encrypt, decrypt} = require('../../../utils/crypt');

passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

passport.use(new GoogleStrategy({
    clientID: googleClientId,
    clientSecret: googleClientSecret,
    callbackURL: googleCallbackUrl
}, (request, accessToken, refreshToken, profile, done) => done(null, profile)));

router
    .get('/login', login)
    .post('/auth', encryptPassword, (request, response, next) => passport.authenticate('google', {
        scope: ['email', 'profile'],
        state: JSON.stringify(request.body.password)
    }, () => next())(request, response, next))
    .get('/callback', passport.authenticate('google', {
        failureRedirect: '/api/auth/provider/google/failure'
    }), decryptPassword, findByEmail, verifyPassword, successLogin)
    .use(badRoute);

function encryptPassword(request, response, next) {
    const {password, email} = request.body;
    const payload = {password, email};
    request.body.password = encrypt(JSON.stringify(payload));

    return next();
}

function decryptPassword(request, response, next) {
    const {user} = request;
    const {state} = request.query;
    const payload = JSON.parse(decrypt(JSON.parse(state)));

    if (user.email !== payload.email)
        return errorResponse(response, 'compare error', 'email wrong');

    request.body = {email: user.email, password: payload.password};

    return next();
}

function login(request, response) {
    return response.send(loginForm);
}

function successLogin(request, response) {
    return successResponse(response);
}

module.exports = router;