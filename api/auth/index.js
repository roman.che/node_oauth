const express = require('express')
const router = express.Router();
const providers = require('./providers');
const {badRoute} = require('../../common/middleware');

router
    .use('/provider', providers)
    .use(badRoute);

module.exports = router;