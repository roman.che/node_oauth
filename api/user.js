const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../models/User');
const {create} = require('../common/crud');
const {onlyAdmin, badRoute} = require('../common/middleware');
const {errorResponse} = require('../common/response');
const {saltRounds} = require('../config');

router
    .use(onlyAdmin)
    .post('/', handlePassword, create(User))
    .use(badRoute);

function handlePassword(request, response, next) {
    const {password, ...body} = request.body;
    if (!password || password.length < 1) {
        request.body = body;
        return next();
    }
    if (password.length < 6) {
        return errorResponse(response, `invalid password`, `password is too shrot`);
    }
    bcrypt.hash(password, saltRounds, (error, hash) => {
        if (error)
            return errorResponse(response, error, `password error`);
        const data = {...body, password: hash};
        request.body = data;
        return next();
    });
}

module.exports = router;