const {errorResponse} = require('./response');
const User = require('../models/User');
const bcrypt = require('bcrypt');

function badRoute(request, response, _) {
    return errorResponse(response, `bad route`, `route not found`, 404);
}

function onlyAdmin(request, response, next) {
    if (request?.body?.type === 'admin')
        return next();
    return invalidToken(request, response);
}

function invalidToken(request, response) {
    const errorMessage = 'Invalid token';
    const userText = JSON.stringify(request.user);
    const error = `${errorMessage} Error - user: ${userText}, IP:${request.ip}`;
    return errorResponse(response, error, errorMessage, 401);
}

function findByEmail(request, response, next) {
    const {email, password} = request.body;
    User.findOne({email}, '+password', {lean: true}, (error, data) => {
        if (error || !data)
            return errorResponse(response, 'invalid login', 'invalid password or email')
        request.body = {unhashedPassword: password, ...data};
        return next();
    });
}

function verifyPassword(request, response, next) {
    const {unhashedPassword, password, ...data} = request.body;
    bcrypt.compare(unhashedPassword, password, (error, same) => {
        if (same) {
            request.body = data;
            return next();
        } else {
            return errorResponse(response, 'compare error', 'password error');
        }
    });
}

module.exports = {onlyAdmin, badRoute, findByEmail, verifyPassword};