function errorResponse(response, error, errorMessage = `failed`, statusCode = 500) {
    console.error(`ERROR: `, error);
    return response.status(statusCode).json({success: false, error: errorMessage});
}

function successResponse(response, data = {}, statusCode = 200) {
    return response.status(statusCode).json({success: true, data});
}

function errorData(response, errorMessage = `failed`) {
    return (error, data) => {
        if (error) return errorResponse(response, error, errorMessage);
        return successResponse(response, data);
    }
}

module.exports = {errorData, errorResponse, successResponse};