const {errorData, errorResponse, successResponse} = require('./response');

function create(model, populate = []) {
    return (request, response) => {

        const newData = new model({
            ...request.body
        });

        return newData
            .save()
            .then(data =>
                populate.length > 0
                    ? data.populate(...populate, errorData(response))
                    : successResponse(response, data))
            .catch(error => errorResponse(response, error));
    };
}

module.exports = {create};