const loginForm = `
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="container">
<h1 class="text-center mt-3 mb-3">oAuth2</h1>
<div class="card">
<div class="card-header">Login</div>
<div class="card-body">
<form method="POST" action="/api/auth/provider/google/auth">

<div class="mb-3">
<label>Email Address</label>
<input type="text" name="email" id="email" class="form-control" />
</div>
<div class="mb-3">
<label>Password</label>
<input type="password" name="password" id="password" class="form-control" />
</div>
<div class="mb-3">
<input type="submit" name="submit_button" class="btn btn-primary" value="Add" />
</div>
</form>
</div>
</div>
</div>
`

module.exports = {loginForm};