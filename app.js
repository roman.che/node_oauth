const express = require('express');
const api = require('./api');
const mongoose = require('mongoose');
const session = require('express-session');
const {mongoUrl} = require('./config');
const app = express();

mongoose.connect(mongoUrl);

app
    .use(express.json())
    .use(express.urlencoded({extended: true}))
    .use(session({
        secret: 'secret',
        resave: false,
        saveUninitialized: true,
        cookie: {secure: true}
    }))
    .use('/api', api);

module.exports = app;